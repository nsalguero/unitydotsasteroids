using Unity.Entities;
using Unity.Physics;

[UpdateInGroup(typeof(LateSimulationSystemGroup))]
public class HitSystem : SystemBase
{
    private BeginSimulationEntityCommandBufferSystem m_BeginSimECB;

    protected override void OnCreate()
    {
        m_BeginSimECB = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var settings = GetSingleton<GameSettingsComponent>();
        var commandBuffer = m_BeginSimECB.CreateCommandBuffer();

        Entities
            .WithName("HitSystem")
            .WithAll<HitTag>()
            .ForEach((Entity e, ref PhysicsVelocity velocity) =>
            {
                var vel = new PhysicsVelocity { Linear = velocity.Linear * -1 };
                commandBuffer.SetComponent(e, vel);
                commandBuffer.RemoveComponent<HitTag>(e);
            }).Run();

        m_BeginSimECB.AddJobHandleForProducer(Dependency);
    }
}

public class HitBulletUfoSystem : SystemBase
{
    private BeginSimulationEntityCommandBufferSystem m_BeginSimECB;

    protected override void OnCreate()
    {
        m_BeginSimECB = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var settings = GetSingleton<GameSettingsComponent>();
        var commandBuffer = m_BeginSimECB.CreateCommandBuffer();

        Entities
            .WithName("HitBulletSystem")
            .WithAll<HitBulletTag>()
            .ForEach((Entity e, ref PlayerComponent player) =>
            {
                if (!player.shield)
                {
                    player.lives -= 1;
                    player.stunTime = settings.stunTime;
                }
                commandBuffer.RemoveComponent<HitBulletTag>(e);
            }).Run();

        m_BeginSimECB.AddJobHandleForProducer(Dependency);
    }
}