using Unity.Entities;

[UpdateInGroup(typeof(LateSimulationSystemGroup))]
public class UserInterfaceSystem : SystemBase
{
    private Entity _entity;
    private UserInterfaceAuthoringComponent _userInterface;

    protected override void OnStartRunning()
    {
        _entity = GetSingletonEntity<UserInterfaceAuthoringComponent>();
        _userInterface = EntityManager.GetComponentData<UserInterfaceAuthoringComponent>(_entity);
    }


    protected override void OnUpdate()
    {
        var playerCount = GetEntityQuery(ComponentType.ReadWrite<PlayerTag>()).CalculateEntityCountWithoutFiltering();

        if (MainSystem.main.inGame &&playerCount != 0)
        {
            var player = GetSingleton<PlayerComponent>();
            _userInterface.instructionsUI.enabled = false;
            _userInterface.scoreUI.enabled = true;
            _userInterface.scoreUI.text = "Score: " + MainSystem.main.currentScore + " - Max Score: " + MainSystem.main.maxScore;

            _userInterface.livesUI.enabled = true;
            _userInterface.livesUI.text = "Lifes: " + player.lives;

            _userInterface.asteroidsUI.enabled = true;
            _userInterface.asteroidsUI.text = "Asteroids: " + MainSystem.main.asteroidsCount;
        }
        else
        {
            if(MainSystem.main.currentScore != 0)
            {
                if (MainSystem.main.win)
                {
                    _userInterface.winUI.enabled = true;
                    _userInterface.loseUI.enabled = false;
                    _userInterface.asteroidsUI.text = "Asteroids: " + MainSystem.main.asteroidsCount;
                }
                else
                {
                    _userInterface.winUI.enabled = false;
                    _userInterface.loseUI.enabled = true;
                }
            }
            else
            {
                _userInterface.instructionsUI.enabled = true;
                _userInterface.winUI.enabled = false;
                _userInterface.loseUI.enabled = false;
                _userInterface.scoreUI.enabled = false;
                _userInterface.livesUI.enabled = false;
                _userInterface.asteroidsUI.enabled = false;
            }
        }
    }
}