using Unity.Burst;
using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;
using System.Diagnostics;
using Unity.Physics;
using Unity.Physics.Systems;
using SphereCollider = Unity.Physics.SphereCollider;
using Unity.Rendering;

[UpdateInGroup(typeof(LateSimulationSystemGroup))]
public class SpawnerAsteroidSystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem m_EndSimEcb;
    private Entity m_AsteroidPrefab, m_PowerUpShieldPrefab, m_DestroyParticlesPref, m_PowerUpShootPrefab, m_PowerUpLifePrefab;
    protected override void OnCreate()
    {
        m_EndSimEcb = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        if (m_AsteroidPrefab == Entity.Null)
        {
            m_AsteroidPrefab = GetSingleton<AsteroidAuthoringComponent>().Prefab;
            m_PowerUpShieldPrefab = GetSingleton<PowerUpAuthoringComponent>().Shield;
            m_PowerUpShootPrefab = GetSingleton<PowerUpAuthoringComponent>().Shoot;
            m_PowerUpLifePrefab = GetSingleton<PowerUpAuthoringComponent>().life;
            m_DestroyParticlesPref = GetSingleton<DestroyAuthoringComponent>().Prefab;
            return;
        }

        var gameSettings = GetSingleton<GameSettingsComponent>();
        var commandBuffer = m_EndSimEcb.CreateCommandBuffer().AsParallelWriter();
        var asteroidPref = m_AsteroidPrefab;
        var powerUpShieldPref = m_PowerUpShieldPrefab;
        var powerUpShootPref = m_PowerUpShootPrefab;
        var powerUpLifePref = m_PowerUpLifePrefab;
        var destroyPref = m_DestroyParticlesPref;

        var rand = new Unity.Mathematics.Random((uint)Stopwatch.GetTimestamp());
        Entities
        .WithAll<SpawnerTag, AsteroidTag>()
        .WithNone<DestroyTag>()
        .ForEach((Entity entity, int nativeThreadIndex, ref Translation position,
                ref AsteroidLvlComponent asteroid, in Rotation rotation,
                in SpawnOffsetComponent spawnOffset) =>
        {
            var randomVel = new Vector3(rand.NextFloat(-1f, 1f), 0, rand.NextFloat(-1f, 1f));
            if (asteroid.lvl > 1)
            {
                asteroid.lvl -= 1;
                //ASTEROIDS
                for (int i = 0; i < 2; i++)
                {
                    var newAsteroid = commandBuffer.Instantiate(nativeThreadIndex, asteroidPref);
                    var asteroidPos = new Translation { Value = position.Value + math.mul(rotation.Value, new float3(rand.NextFloat(-1,1), 0, rand.NextFloat(-1, 1))).xyz };
                    
                    commandBuffer.SetComponent(nativeThreadIndex, newAsteroid, asteroidPos);

                    randomVel = new Vector3(rand.NextFloat(-1f, 1f), 0, rand.NextFloat(-1f, 1f));
                    randomVel.Normalize();
                    randomVel = randomVel * gameSettings.asteroidVelocity;
                    var vel = new PhysicsVelocity { Linear = new float3(randomVel.x, randomVel.y, randomVel.z) };
                    commandBuffer.SetComponent(nativeThreadIndex, newAsteroid, vel);

                    var a = new AsteroidLvlComponent { lvl = asteroid.lvl };
                    commandBuffer.SetComponent(nativeThreadIndex, newAsteroid, a);

                }
                
                //POWER UP
                var randomGenerator = rand.NextInt(0, 100);
                if (randomGenerator < 45)
                {
                    var randomType = rand.NextInt(1, 4);
                    var type = new PU_Type();
                    var newPowerUp = new Entity();

                    switch (randomType)
                    {
                        case 1:
                            type = PU_Type.Life;
                            newPowerUp = commandBuffer.Instantiate(nativeThreadIndex, powerUpLifePref);
                            break;
                        case 2:
                            type = PU_Type.Shield;                            
                            newPowerUp = commandBuffer.Instantiate(nativeThreadIndex, powerUpShieldPref);
                            break;
                        case 3:
                            type = PU_Type.Shot;
                            newPowerUp = commandBuffer.Instantiate(nativeThreadIndex, powerUpShootPref);
                            break;
                    }

                    var powerUpPos = new Translation { Value = position.Value };
                    commandBuffer.SetComponent(nativeThreadIndex, newPowerUp, powerUpPos);

                    randomVel = new Vector3(rand.NextFloat(-1f, 1f), 0, rand.NextFloat(-1f, 1f));
                    randomVel.Normalize();
                    var powerUpVel = new PhysicsVelocity { Linear = new float3(randomVel.x, randomVel.y, randomVel.z) };
                    commandBuffer.SetComponent(nativeThreadIndex, newPowerUp, powerUpVel);


                    var powerUpConfig = new PowerUpComponent { type = type };
                    commandBuffer.SetComponent(nativeThreadIndex, newPowerUp, powerUpConfig);

                }
                commandBuffer.DestroyEntity(nativeThreadIndex, entity);
            }
            else
            {
                commandBuffer.DestroyEntity(nativeThreadIndex, entity);
            }
            var DestroyParticles = commandBuffer.Instantiate(nativeThreadIndex, destroyPref);
            var newPos = new Translation { Value = position.Value };
            commandBuffer.SetComponent(nativeThreadIndex, DestroyParticles, newPos);

        }).WithBurst().ScheduleParallel();

        m_EndSimEcb.AddJobHandleForProducer(Dependency);
    }
}