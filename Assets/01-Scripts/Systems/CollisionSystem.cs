using System.Diagnostics;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Stateful;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
[UpdateAfter(typeof(TriggerEventConversionSystem))]
public class CollisionBulletSystem : SystemBase
{
    private EndFixedStepSimulationEntityCommandBufferSystem m_CommandBufferSystem;

    private TriggerEventConversionSystem m_TriggerSystem;
    private EntityQueryMask m_NonTriggerMask;

    private Entity m_DestroyParticlesPref;

    protected override void OnCreate()
    {
        m_CommandBufferSystem = World.GetOrCreateSystem<EndFixedStepSimulationEntityCommandBufferSystem>();
        m_TriggerSystem = World.GetOrCreateSystem<TriggerEventConversionSystem>();
        m_NonTriggerMask = EntityManager.GetEntityQueryMask(
            GetEntityQuery(new EntityQueryDesc
            {
                None = new ComponentType[]
                {
                    typeof(StatefulTriggerEvent)
                }
            })
        );
        RequireSingletonForUpdate<DestroyAuthoringComponent>();
    }

    protected override void OnUpdate()
    {
        if (m_DestroyParticlesPref == Entity.Null)
        {
            m_DestroyParticlesPref = GetSingleton<DestroyAuthoringComponent>().Prefab;
            return;
        }

        Dependency = JobHandle.CombineDependencies(m_TriggerSystem.OutDependency, Dependency);

        var commandBuffer = m_CommandBufferSystem.CreateCommandBuffer();

        var nonTriggerMask = m_NonTriggerMask;
        var gameSettings = GetSingleton<GameSettingsComponent>();
        var rand = new Unity.Mathematics.Random((uint)Stopwatch.GetTimestamp());
        var destroyPref = m_DestroyParticlesPref;

        Entities
            .WithName("CollisionBulletSystem")
            .WithoutBurst()
            .WithAll<BulletTag>()
            .WithNone<HitTag>()
            .ForEach((Entity e, ref DynamicBuffer<StatefulTriggerEvent> triggerEventBuffer, 
                    in Translation position) =>
            {                
                for (int i = 0; i < triggerEventBuffer.Length; i++)
                {
                    var triggerEvent = triggerEventBuffer[i];
                    var otherEntity = triggerEvent.GetOtherEntity(e);

                    if (triggerEvent.State == EventOverlapState.Stay || !nonTriggerMask.Matches(otherEntity))
                    {
                        continue;
                    }
                    if (triggerEvent.State == EventOverlapState.Enter)
                    {
                        var randomGenerator = rand.NextInt(gameSettings.asteroidScore.x, gameSettings.asteroidScore.y);
                        MainSystem.main.currentScore += randomGenerator;

                        if (EntityManager.HasComponent<AsteroidTag>(otherEntity))
                        {
                            commandBuffer.AddComponent(otherEntity, new SpawnerTag { });
                        }
                        
                        if (EntityManager.HasComponent<UfoTag>(otherEntity))
                        {
                            UfoSystem.main.currentTime = 0;
                            commandBuffer.AddComponent(otherEntity, new DestroyTag { });
                            var DestroyParticles = commandBuffer.Instantiate(destroyPref);
                            var newPos = new Translation { Value = position.Value };
                            commandBuffer.SetComponent(DestroyParticles, newPos);
                        }
                        commandBuffer.DestroyEntity(e);                        
                    }
                }
            }).Run();

        Entities
            .WithName("CollisitonBulletUFOSystem")
            .WithoutBurst()
            .WithAll<BulletUFOTag>()
            .WithNone<HitTag>()
            .ForEach((Entity e, ref DynamicBuffer<StatefulTriggerEvent> triggerEventBuffer, 
                     in Translation position) =>
            {
                for (int i = 0; i < triggerEventBuffer.Length; i++)
                {
                    var triggerEvent = triggerEventBuffer[i];
                    var otherEntity = triggerEvent.GetOtherEntity(e);
                    if (triggerEvent.State == EventOverlapState.Enter)
                    {
                        if (EntityManager.HasComponent<BulletTag>(otherEntity))
                        {
                            var randomGenerator = rand.NextInt(gameSettings.asteroidScore.x, gameSettings.asteroidScore.y);
                            MainSystem.main.currentScore += randomGenerator;
                            commandBuffer.AddComponent(otherEntity, new DestroyTag { });

                            var DestroyParticles = commandBuffer.Instantiate(destroyPref);
                            var newPos = new Translation { Value = position.Value };
                            commandBuffer.SetComponent(DestroyParticles, newPos);
                        }
                        else
                        {           
                            commandBuffer.AddComponent(otherEntity, new HitBulletTag());
                        }
                        commandBuffer.DestroyEntity(e);
                        return;
                    }
                }
            }).Run();

        Entities
            .WithName("CollisionPlayerSystem")
            .WithoutBurst()
            .WithAll<PlayerTag>()
            .WithNone<HitTag>()
            .ForEach((Entity e, ref DynamicBuffer<StatefulTriggerEvent> triggerEventBuffer,
                     ref PlayerComponent player) =>
            {
                for (int i = 0; i < triggerEventBuffer.Length; i++)
                {
                    var triggerEvent = triggerEventBuffer[i];
                    var otherEntity = triggerEvent.GetOtherEntity(e);

                    if (triggerEvent.State == EventOverlapState.Stay || !nonTriggerMask.Matches(otherEntity))
                    {
                        continue;
                    }                                       

                    if (EntityManager.HasComponent<AsteroidTag>(otherEntity))
                    {
                        if (player.stunTime < 0)
                        {
                            if (triggerEvent.State == EventOverlapState.Enter)
                            {
                                if (!player.hit && !player.shield)
                                {
                                    player.hit = true;
                                    player.lives -= 1;
                                    player.stunTime = gameSettings.stunTime;
                                    commandBuffer.AddComponent(e, new HitTag());
                                    commandBuffer.AddComponent(otherEntity, new HitTag());
                                }
                                return;
                            }
                        }

                        if (triggerEvent.State == EventOverlapState.Exit)
                        {
                            player.hit = false;
                        }
                    }

                    if (EntityManager.HasComponent<PowerUpTag>(otherEntity))
                    {
                        PowerUpComponent powerUp = EntityManager.GetComponentData<PowerUpComponent>(otherEntity);
                        if (triggerEvent.State == EventOverlapState.Enter)
                        {
                            if (!powerUp.hit)
                            {
                                switch (powerUp.type)
                                {
                                    case PU_Type.Life:
                                        player.lives += 1;
                                        break;
                                    case PU_Type.Shield:
                                        player.shieldTime = 0;
                                        player.shield = true;
                                        break;
                                    case PU_Type.Shot:
                                        player.typeShot = 1;
                                        player.timePuShot = 0;
                                        break;
                                    default:
                                        break;
                                }
                                powerUp.hit = true;
                                commandBuffer.DestroyEntity(otherEntity);
                                return;
                            }
                        }

                        if (triggerEvent.State == EventOverlapState.Exit)
                        {
                            powerUp.hit = false;
                        }
                    }
                }
            }).Run();

        m_CommandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}
