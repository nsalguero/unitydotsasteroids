using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class LookAtSystem: JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        JobHandle handle = new JobHandle();
        if(MainSystem.main.inGame)
        {
            var player = GetSingleton<PlayerComponent>();

            handle = Entities.WithAny<LookAtTag>().ForEach((ref Rotation rot, in Translation pos) =>
            {
                float3 dir = player.currentPos - pos.Value;
                rot.Value = quaternion.LookRotation(math.normalize(dir), math.up());
            }).Schedule(inputDeps);
        
            handle.Complete();
        }
        return handle;
    }
}
