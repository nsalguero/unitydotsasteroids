using Unity.Entities;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Unity.Burst;
using Unity.Physics;

public class InputSystem : SystemBase
{
    private EntityQuery m_BulletQuery;

    private BeginSimulationEntityCommandBufferSystem m_BeginSimECB;
    private Entity m_BulletPrefab;
    
    private bool inGame;

    protected override void OnCreate()
    {
        m_BulletQuery = GetEntityQuery(ComponentType.ReadWrite<BulletTag>());

        m_BeginSimECB = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
        RequireSingletonForUpdate<GameSettingsComponent>();
    }

    protected override void OnUpdate()
    {
        if (m_BulletPrefab == Entity.Null)
        {
            m_BulletPrefab = GetSingleton<BulletAuthoringComponent>().Prefab;
            return;
        }

        var gameSettings = GetSingleton<GameSettingsComponent>();

        if (Input.GetKey(KeyCode.P))
        {
            inGame = true;
        }
        
        if (!inGame)
        {
            return;
        }

        var commandBuffer2 = m_BeginSimECB.CreateCommandBuffer().AsParallelWriter();
        var bulletCount = m_BulletQuery.CalculateEntityCountWithoutFiltering();
        var bulletPrefab = m_BulletPrefab;
        var deltaTime = Time.DeltaTime;

        byte thrust, reverseThrust, tryShoot;
        thrust = reverseThrust = tryShoot = 0;

        byte rightRot, leftRot;
        rightRot = leftRot = 0;
        

        if (Input.GetKey(KeyCode.UpArrow))
        {
            thrust = 1;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            reverseThrust = 1;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            leftRot = 1;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rightRot = 1;
        }

        if (Input.GetKey(KeyCode.Space))
        {
            tryShoot = 1;
        }


        Entities.WithAll<PlayerTag>().ForEach((Entity entity, int nativeThreadIndex, ref Rotation rotation, 
            ref PhysicsVelocity  velocity, 
            ref PlayerComponent player,
            ref Translation position, in SpawnOffsetComponent bulletOffset) =>
        {
            //Movement
            /*if (right == 1)
            {
                velocity.Linear += (math.mul(rotation.Value, new float3(1, 0, 0)).xyz) * gameSettings.playerForce * deltaTime;
            }
            if (left == 1)
            {
                velocity.Linear += (math.mul(rotation.Value, new float3(-1, 0, 0)).xyz) * gameSettings.playerForce * deltaTime;
            }*/


            if (thrust == 1)
            {
                velocity.Linear += (math.mul(rotation.Value, new float3(0, 0, 1)).xyz) * gameSettings.playerForce * deltaTime;
            }

            if (reverseThrust == 1)
            {
                velocity.Linear += (math.mul(rotation.Value, new float3(0, 0, -1)).xyz) * gameSettings.playerForce * deltaTime;
            }
            player.currentPos = position.Value;

            //Rotation
            if (leftRot == 1)
            {
                player.rotationSpeed -= gameSettings.playerRotationSpeed * deltaTime;
                player.rotationSpeed = Mathf.Clamp(player.rotationSpeed, -1, 1);
            }

            if (rightRot == 1)
            {
                player.rotationSpeed += gameSettings.playerRotationSpeed * deltaTime;
                player.rotationSpeed = Mathf.Clamp(player.rotationSpeed, -1, 1);
            }

            if(rightRot == 1 || leftRot == 1)
                velocity.Angular = new float3(0, player.rotationSpeed, 0);

            //Shield
            player.shieldTime += deltaTime;
            if (player.shield && player.shieldTime >= player.maxShieldTime)
            {
                player.shield = false;
            }

            player.stunTime -= deltaTime;

            //Attack
            player.currentDelay += deltaTime;
            if(player.currentDelay <= player.shootDelay && player.typeShot == 0)
            {
                return;
            }

            if(player.typeShot == 1)
            {
                player.timePuShot += deltaTime;
                if (player.timePuShot >= player.maxTimePuShot)
                {
                    player.typeShot = 0;
                    return;
                }
            }

            if (tryShoot == 1) 
            {
                var bulletEntity = commandBuffer2.Instantiate(nativeThreadIndex, bulletPrefab);

                var newPosition = new Translation { Value = position.Value + math.mul(rotation.Value, bulletOffset.pos).xyz };
                commandBuffer2.SetComponent(nativeThreadIndex, bulletEntity, newPosition);

                var newRotation = new Rotation { Value = rotation.Value};
                commandBuffer2.SetComponent(nativeThreadIndex, bulletEntity, newRotation);

                var vel = new PhysicsVelocity { Linear = (gameSettings.bulletVelocity * math.mul(rotation.Value, new float3(0, 0, 1)).xyz) + velocity.Linear };
                commandBuffer2.SetComponent(nativeThreadIndex, bulletEntity, vel);
                player.currentDelay = 0f;
            }


        }).ScheduleParallel();

        Entities.WithAll<ControlPosTag>().
            ForEach((ref Translation position) => {
                position.Value.y = -1;
            }).ScheduleParallel();

        m_BeginSimECB.AddJobHandleForProducer(Dependency);
    }
}