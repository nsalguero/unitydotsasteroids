using Unity.Entities;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Unity.Burst;
using Unity.Physics;
using System.Diagnostics;
using Unity.Rendering;

public class MainSystem : SystemBase
{
    public EntityQuery m_PlayerQuery, m_AsteroidQuery, m_BulletQuery, m_shieldQuery;

    private BeginSimulationEntityCommandBufferSystem m_BeginSimECB;
    private Entity m_PlayerPrefab, m_AsteroidPrefab, m_ShieldPrefab;

    public bool inGame, finish, win = false;

    public int currentScore;
    public int maxScore;
    public int asteroidsCount;

    public static MainSystem main;

    protected override void OnCreate()
    {
        main = this;

        m_PlayerQuery = GetEntityQuery(ComponentType.ReadWrite<PlayerTag>());
        m_AsteroidQuery = GetEntityQuery(ComponentType.ReadWrite<AsteroidTag>());
        m_BulletQuery = GetEntityQuery(ComponentType.ReadWrite<BulletTag>());
        m_shieldQuery = GetEntityQuery(ComponentType.ReadWrite<ShieldTag>());

        m_BeginSimECB = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
        RequireSingletonForUpdate<GameSettingsComponent>();
    }

    protected override void OnUpdate()
    {
        if (m_PlayerPrefab == Entity.Null || m_AsteroidPrefab == Entity.Null || m_ShieldPrefab == Entity.Null)
        {
            m_PlayerPrefab = GetSingleton<PlayerAuthoringComponent>().Ship;
            m_AsteroidPrefab = GetSingleton<AsteroidAuthoringComponent>().Prefab; 
            m_ShieldPrefab = GetSingleton<PlayerAuthoringComponent>().Shield;
            return;
        }

        var gameSettings = GetSingleton<GameSettingsComponent>();
        var commandBuffer = m_BeginSimECB.CreateCommandBuffer();
        var playerCount = m_PlayerQuery.CalculateEntityCountWithoutFiltering();
        var shieldCount = m_shieldQuery.CalculateEntityCountWithoutFiltering();
        asteroidsCount = m_AsteroidQuery.CalculateEntityCountWithoutFiltering();
        
        if(!inGame)
        { 
            byte ini;
            ini = 0;
            if (Input.GetKey(KeyCode.P) && playerCount < 1)
            {
                ini = 1;
            }
            if (Input.GetKey(KeyCode.I) && playerCount < 1)
            {
                currentScore = 0;
            }

            if (ini == 1 && playerCount < 1)
            {
                finish = false;
                win = false;
                currentScore = 0;
                var pos = new Translation { Value = new float3(0, -1, 0) };

                var e = commandBuffer.Instantiate(m_PlayerPrefab);
                commandBuffer.SetComponent(e, pos);

                var p = new PlayerComponent { shootDelay = gameSettings.playerShootDelay,
                                            rotationSpeed = gameSettings.playerRotationSpeed,
                                            lives = gameSettings.playerLives,
                                            maxShieldTime = gameSettings.maxShieldTime,
                                            stunTime = gameSettings.stunTime,
                                            maxTimePuShot = gameSettings.shotTime};

                commandBuffer.SetComponent(e, p);

                var rand = new Unity.Mathematics.Random((uint)Stopwatch.GetTimestamp());

                if (ini == 1 && asteroidsCount < 1)
                {
                    for (int i = asteroidsCount; i < gameSettings.level * gameSettings.numAsteroids; ++i)
                    {
                        var padding = 0.1f;

                        var xPosition = rand.NextFloat(-1f * ((gameSettings.levelWidth) / 2 - padding), (gameSettings.levelWidth) / 2 - padding);
                        var yPosition = -1f;
                        var zPosition = rand.NextFloat(-1f * ((gameSettings.levelDepth) / 2 - padding), (gameSettings.levelDepth) / 2 - padding);

                        pos = new Translation { Value = new float3(xPosition, yPosition, zPosition) };

                        e = commandBuffer.Instantiate(m_AsteroidPrefab);
                        commandBuffer.SetComponent(e, pos);

                        var randomVel = new Vector3(rand.NextFloat(-1f, 1f), 0, rand.NextFloat(-1f, 1f));
                        randomVel.Normalize();
                        randomVel = randomVel * gameSettings.asteroidVelocity;
                        var vel = new PhysicsVelocity { Linear = new float3(randomVel.x, randomVel.y, randomVel.z) };
                        commandBuffer.SetComponent(e, vel);

                        var a = new AsteroidLvlComponent { lvl = gameSettings.level };
                        commandBuffer.SetComponent(e, a);
                    }
                }
                inGame = true;
            }
            return;
        }

        if (asteroidsCount == 0 && !finish)
        {
            finish = true;
            win = true;
            inGame = false;
            Entities.WithAny<PowerUpTag>().ForEach((Entity entity) => {
                commandBuffer.AddComponent(entity, new DestroyTag() { });
            }).Run();

            Entities.WithAny<PlayerTag, BulletTag, UfoTag>().ForEach((Entity entity) => {
                commandBuffer.AddComponent(entity, new DestroyTag() { });
            }).Run();
        }

        if (finish)
        {
            inGame = false;
            Entities.WithAny<AsteroidTag, UfoTag, BulletUFOTag>().ForEach((Entity entity) => {
                commandBuffer.AddComponent(entity, new DestroyTag() { });
            }).Run();

            Entities.WithAny<PlayerTag, PowerUpTag, BulletTag>().ForEach((Entity entity) => {
                commandBuffer.AddComponent(entity, new DestroyTag() { });
            }).Run();

            if (currentScore > maxScore)
                maxScore = currentScore;
        }

        Entities.WithAll<PlayerTag>().ForEach((Entity entity, PlayerComponent player, in Translation position) =>
        {
            if (player.shield && player.shieldTime == 0)
            {
                var shieldEntity = commandBuffer.Instantiate(m_ShieldPrefab);

                var newPos = new Translation() { Value = position.Value };
                commandBuffer.SetComponent(shieldEntity, newPos);
            }

            if (player.lives == 0)
            {
                finish = true;
            }
        }).WithoutBurst().Run();



        m_BeginSimECB.AddJobHandleForProducer(Dependency);

    }
}

[UpdateInGroup(typeof(LateSimulationSystemGroup))]
public class DestroySystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem m_EndSimEcb;

    protected override void OnCreate()
    {
        m_EndSimEcb = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var commandBuffer = m_EndSimEcb.CreateCommandBuffer().AsParallelWriter();

        Entities
        .WithAll<DestroyTag>()
        .ForEach((Entity entity, int nativeThreadIndex) =>
        {
            commandBuffer.DestroyEntity(nativeThreadIndex, entity);

        }).WithBurst().ScheduleParallel();

        m_EndSimEcb.AddJobHandleForProducer(Dependency);
    }
}


