using TMPro;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class SetGameSettingsSystem : UnityEngine.MonoBehaviour, IConvertGameObjectToEntity
{

    [Header("Level Config")]
    public int levelWidth = 80;
    public int levelDepth = 60;
    public int level = 1;

    [Header("Asteroids")]
    public float asteroidVelocity = 3f;
    public int numAsteroids = 2;
    public int2 asteroidScore = new int2(10000, 15000);

    [Header("UFO")]
    public float ufoSpawnTime = 8f;
    public float ufoShotTime = 6f;
    public int ufoVeloc = 6;

    [Header("Player")]
    public float playerMaxVeloc = 10f;
    public float playerForce = 10f;
    public float playerMaxRotSpeed = 10f;
    public float playerRotationSpeed = 25f;
    public float playerShootDelay = 2f;
    public int playerlives = 3;
    public float maxShieldTime = 5;
    public float stunTime = 5;
    public float shotTime = 5;

    [Header("Bullets")]
    public float bulletVelocity = 20f;



    [HideInInspector]
    public bool inGame;
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        var settings = default(GameSettingsComponent);

        settings.asteroidVelocity = asteroidVelocity;
        settings.playerMaxVeloc = playerMaxVeloc;
        settings.playerForce = playerForce;
        settings.playerMaxRotSpeed = playerMaxRotSpeed;
        settings.playerRotationSpeed = playerRotationSpeed;
        settings.bulletVelocity = bulletVelocity;
        settings.playerShootDelay = playerShootDelay;
        settings.playerLives = playerlives;
        settings.level = level;
        settings.maxShieldTime = maxShieldTime;
        settings.asteroidScore = asteroidScore;
        settings.stunTime = stunTime;
        settings.ufoSpawnTime = ufoSpawnTime;
        settings.shotTime = shotTime;
        settings.ufoShotTime = ufoShotTime;
        settings.ufoVeloc = ufoVeloc;

        settings.numAsteroids = numAsteroids;
        settings.levelWidth = levelWidth;
        settings.levelDepth = levelDepth;
        dstManager.AddComponentData(entity, settings);
    }
}