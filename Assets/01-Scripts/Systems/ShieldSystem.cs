using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Unity.Burst;

public class ShieldSystem : SystemBase
{
    private BeginSimulationEntityCommandBufferSystem m_BeginSimECB;

    protected override void OnCreate()
    {
        m_BeginSimECB = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var commandBuffer = m_BeginSimECB.CreateCommandBuffer();

        var newPos = new Translation();
        var shield = false;

        Entities.WithAll<PlayerTag>().ForEach((Entity e, in Translation position, in PlayerComponent player) =>
        {
            newPos.Value = position.Value;
            shield = player.shield;
        }).Run();

        Entities.WithAll<ShieldTag>()
            .ForEach((Entity e, ref Translation position) => {
                if (shield)
                    commandBuffer.SetComponent(e, newPos);
                else
                    commandBuffer.DestroyEntity(e);
            }).Run();
    }
}