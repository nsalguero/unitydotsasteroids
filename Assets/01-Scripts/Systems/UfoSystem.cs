using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;

public class UfoSystem : SystemBase
{
    public static UfoSystem main;

    public EntityQuery m_UfoQuery;

    private Entity m_UfoPrefab, m_BulletEnemyPrefab;
    private BeginSimulationEntityCommandBufferSystem m_BeginSimECB;

    public float currentTime;
    private float currentShotTime;

    protected override void OnCreate()
    {
        main = this;

        m_UfoQuery = GetEntityQuery(ComponentType.ReadWrite<UfoTag>());
        m_BeginSimECB = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
        RequireSingletonForUpdate<GameSettingsComponent>();
    }

    protected override void OnUpdate()
    {
        if(m_UfoPrefab == Entity.Null || m_BulletEnemyPrefab == Entity.Null)
        {
            var ufo = GetSingleton<UfoAuthoringComponent>();
            m_UfoPrefab = ufo.Ship;
            m_BulletEnemyPrefab = ufo.Bullets;
            return;
        }
        
        var ufoCount = m_UfoQuery.CalculateEntityCountWithoutFiltering();
        var deltaTime = Time.DeltaTime;
        var settings = GetSingleton<GameSettingsComponent>();
        var rand = new Unity.Mathematics.Random((uint)Stopwatch.GetTimestamp());

        if (ufoCount == 0 && MainSystem.main.inGame)
        {
            var commandBuffer = m_BeginSimECB.CreateCommandBuffer();
            currentTime += deltaTime;
            if (currentTime >= settings.ufoSpawnTime)
            {
                var Ufo = commandBuffer.Instantiate(m_UfoPrefab);
                var randomSide = rand.NextBool();
                var randomZ = rand.NextFloat(-settings.levelDepth/2, settings.levelDepth/2);
                var ufoPos = new Translation();
                var ufoVel = new PhysicsVelocity();
                var ufoRot = new Rotation();
                var rot = new Quaternion();

                if (randomSide)
                {
                    ufoPos = new Translation { Value = new float3(settings.levelWidth / 2, -1, randomZ) };
                    ufoVel = new PhysicsVelocity { Linear = new float3(-settings.ufoVeloc, 0, 0) };
                    rot.eulerAngles = new Vector3(0, -90, 0);
                }
                else
                {
                    ufoPos = new Translation { Value = new float3(-settings.levelWidth / 2, -1, randomZ) };
                    ufoVel = new PhysicsVelocity { Linear = new float3(settings.ufoVeloc, 0, 0) };
                    rot.eulerAngles = new Vector3(0, 90, 0);
                }

                ufoRot = new Rotation { Value = rot };
                commandBuffer.SetComponent(Ufo, ufoPos);
                commandBuffer.SetComponent(Ufo, ufoVel);
                commandBuffer.SetComponent(Ufo, ufoRot);
                currentShotTime = settings.ufoShotTime;
            }
            return;
        }
        
        if (ufoCount != 0 && MainSystem.main.inGame)
        {
            currentTime = 0;
            currentShotTime -= deltaTime;            
            Entities.WithAll<UfoTag>().ForEach((Entity entity, int nativeThreadIndex, ref PhysicsVelocity velocity, ref Rotation rotation, ref Translation position) =>
            {
                var commandBuffer = m_BeginSimECB.CreateCommandBuffer();

                if (currentShotTime <= 0)
                {
                    var bullet = commandBuffer.Instantiate(m_BulletEnemyPrefab);
                    var newPos = new Translation { Value = position.Value };
                    commandBuffer.SetComponent(bullet, newPos);

                    var newRotation = new Rotation { Value = rotation.Value };
                    commandBuffer.SetComponent(bullet, newRotation);

                    var vel = new PhysicsVelocity { Linear = (settings.bulletVelocity * math.mul(rotation.Value, new float3(0, 0, 1)).xyz) + velocity.Linear };
                    commandBuffer.SetComponent(bullet, vel);
                    currentShotTime = settings.ufoShotTime;
                }

                if (position.Value.x < (-settings.levelWidth / 2) - 10 || position.Value.x > (settings.levelWidth / 2) + 10)
                    commandBuffer.DestroyEntity(entity);

            }).WithoutBurst().Run();
            return;
        }     
    }


}
