using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Unity.Burst;

public class WarpSystem : SystemBase
{
    private BeginSimulationEntityCommandBufferSystem m_BeginSimECB;

    protected override void OnCreate()
    {
        m_BeginSimECB = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var settings = GetSingleton<GameSettingsComponent>();
        var deltaTime = Time.DeltaTime;

        var commandBuffer = m_BeginSimECB.CreateCommandBuffer().AsParallelWriter();

        if (MainSystem.main.inGame)
        {
            Entities.WithAll<WarpTag>().WithNone<DestroyTag>().ForEach((Entity e, int nativeThreadIndex, ref Translation position, ref WarpComponent warp) =>
            {           
                warp.time -= deltaTime;
                if (warp.time <= 0)
                {
                    if (Mathf.Abs(position.Value.x) > settings.levelWidth / 2)
                    {
                        position.Value = new float3(-position.Value.x + 3,0,position.Value.z);
                        warp.time = 3;
                    }else if(Mathf.Abs(position.Value.x) < -settings.levelWidth / 2)
                    {
                        position.Value = new float3(position.Value.x - 3,0,position.Value.z);
                        warp.time = 3;
                    }

                    if (Mathf.Abs(position.Value.z) > settings.levelDepth / 2)                    
                    {
                        position.Value = new float3(position.Value.x, 0, -position.Value.z + 3);
                        warp.time = 3;
                    }
                    else if(Mathf.Abs(position.Value.z) < -settings.levelDepth / 2)
                    {
                        position.Value = new float3(position.Value.x + 3, 0, position.Value.z - 3);
                        warp.time = 3;
                    }
                }

            }).ScheduleParallel();
        }

        m_BeginSimECB.AddJobHandleForProducer(Dependency);
    }
}