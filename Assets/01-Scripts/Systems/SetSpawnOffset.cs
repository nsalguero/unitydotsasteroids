using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class SetSpawnOffset : UnityEngine.MonoBehaviour, IConvertGameObjectToEntity
{
    public GameObject prefSpawn;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        var prefOffset = default(SpawnOffsetComponent);

        var offsetVector = prefSpawn.transform.position;
        prefOffset.pos = new float3(offsetVector.x, offsetVector.y, offsetVector.z);

        dstManager.AddComponentData(entity, prefOffset);
    }
}