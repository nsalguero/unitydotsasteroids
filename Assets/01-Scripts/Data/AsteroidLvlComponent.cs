using Unity.Entities;

[GenerateAuthoringComponent]
public struct AsteroidLvlComponent : IComponentData
{
    public AsteroidLvlComponent(float lvl)
    {
        this.lvl = lvl;
    }

    public float lvl;
}