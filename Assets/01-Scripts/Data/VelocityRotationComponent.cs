using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct VelocityRotationComponent : IComponentData
{
    public float Value;
}
