using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateAuthoringComponent]
public struct PlayerComponent : IComponentData
{
    public float3 currentPos;

    public float shootDelay;
    public float currentDelay;
    public bool shoot;
    public byte typeShot;
    public float timePuShot;
    public float maxTimePuShot;

    public float rotationSpeed;
    public int lives;
    public bool shield;
    public float shieldTime;
    public bool hit;
    public float maxShieldTime;

    public float stunTime;
}