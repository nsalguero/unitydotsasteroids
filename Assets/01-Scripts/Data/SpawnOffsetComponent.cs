using Unity.Entities;
using Unity.Mathematics;

public struct SpawnOffsetComponent : IComponentData
{
    public float3 pos;
}
