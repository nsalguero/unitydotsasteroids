using Unity.Entities;

[GenerateAuthoringComponent]
public struct DestroyAuthoringComponent : IComponentData
{
    public Entity Prefab;
}