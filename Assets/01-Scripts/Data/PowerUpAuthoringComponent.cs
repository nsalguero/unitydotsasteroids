using Unity.Entities;

[GenerateAuthoringComponent]
public struct PowerUpAuthoringComponent : IComponentData
{
    public Entity Shield;
    public Entity Shoot;
    public Entity life;
}