using Unity.Entities;
using TMPro;

[GenerateAuthoringComponent]
public class UserInterfaceAuthoringComponent : IComponentData
{
    public TMP_Text scoreUI;
    public TMP_Text instructionsUI;
    public TMP_Text livesUI;
    public TMP_Text winUI;
    public TMP_Text loseUI;
    public TMP_Text asteroidsUI;
}
