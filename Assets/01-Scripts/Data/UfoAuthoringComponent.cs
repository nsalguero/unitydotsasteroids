using Unity.Entities;

[GenerateAuthoringComponent]
public struct UfoAuthoringComponent : IComponentData
{
    public Entity Ship;
    public Entity Bullets;
}