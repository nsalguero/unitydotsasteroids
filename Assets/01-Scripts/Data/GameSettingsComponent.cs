using Unity.Entities;
using TMPro;
using Unity.Mathematics;

public struct GameSettingsComponent : IComponentData
{
    //Asteroids
    public float asteroidVelocity;
    public int numAsteroids;
    public int2 asteroidScore;

    //UFO
    public float ufoSpawnTime;
    public float ufoShotTime;
    public int ufoVeloc;

    //Player
    public float playerMaxVeloc;
    public float playerForce;
    public float playerMaxRotSpeed;
    public float playerRotationSpeed;
    public float playerShootDelay;
    public int playerLives;
    public float maxShieldTime;
    public float stunTime;
    public float shotTime;
    
    //Bullets
    public float bulletVelocity;

    //Level Settings    
    public int levelWidth;
    public int levelDepth;
    public int level;
}