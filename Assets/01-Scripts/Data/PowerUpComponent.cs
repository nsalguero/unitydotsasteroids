using Unity.Entities;
using Unity.Mathematics;

public enum PU_Type
{
    Life,
    Shield,
    Shot
}

[GenerateAuthoringComponent]
public struct PowerUpComponent : IComponentData
{
    public float time;
    public PU_Type type;
    public bool hit;
}
